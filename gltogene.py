"""
# file : gltogene.py
# 
# history: 
#     author: bue
#     date: 2014-04-29 
#     license: >= GPL3 
#     language: Python 3.4
#     description:
#     handling standard  gene, igv and circos tracks
#
"""

# load libraries
import linecache
import os
import re 
import statistics
import sys
import pyglue.glue as gl


### off shore ##
def txtigv2toolsortigv(s_file,):
    """
    txtigv2toolsortigv - sort generated igv file by the sort igvtool 
    dependency: 
        igvtools installed which can be downloaded here: http://www.broadinstitute.org/software/igv/download
    input : 
        s_file : prefix for '_glue.txt' igv input file which will result in prefix '_igvtools_glue.igv' output fiel   rdbxgene compatibel input file  
    output : 
        sorted igv file 
    """
    print("i txtigv2toolsortigv s_file :", s_file)
    # handle input 
    s_filein = s_file+'_glue.txt'
    s_fileprocess = s_file+'_glue.igv'
    s_fileout = s_file+'_igvtools_glue.igv'
    # process
    os.rename(s_filein, s_fileprocess)  # substitute origina input txt by igv extension
    s_command = 'igvtools sort '+s_fileprocess+' '+s_fileout # igvtool sort igv file
    os.system(s_command)
    os.remove(s_fileprocess)  # get rid of intermediate version
    # outout
    print("o txtigv2toolsortigv s_fileout :", s_fileout)
    return(s_fileout)


# gene meadin centric over a cohort of sample 
def txtgene2genecentric(ls_inputfile=[], tivts_key=('gene_id',), ivs_value='value', ivs_feature=None, ivs_labelrow=None, ivs_headerrow=None, method=statistics.median):
    """
    txtgene2genecentric - convert a set of gene tracks or igv tracks into gene or chomosome_start_end centric tracks respective
    input :
        ls_inputfile : list of gene tack file names or list of igv track file names 
        tivts_key : tupe of string or integer points to the column holding the ('gene_id') or pointing to the columns holding the ('chomosome','start','end',) key. first colum ins 0.
        ivs_value : string or integer pointing to the column holding the 'value'
        ivs_feature : string or integer pointing to the column holding the 'feature's. None = no feature column
        ivs_labelrow : integer or string or None. first row is 0. default None is no label row 
        ivs_headerrow : integer or string or None. first row is 0. default None is no label row 
        method : averageing methode used for calculation. e.g. statistics.mean, statistics.median. 
    output : 
        ls_outputfile : list of generated centric tack file names
        gene centric gene track files or chomosome_start_end centric igv track files 
    """
    print("i txtgene2genecentric ls_inputfile :", ls_inputfile)
    print("i txtgene2genecentric tivts_key :", tivts_key)
    print("i txtgene2genecentric ivs_value :", ivs_value)
    print("i txtgene2genecentric ivs_feature :", ivs_feature)
    print("i txtgene2genecentric ivs_labelrow :", ivs_labelrow)
    print("i txtgene2genecentric ivs_headerrow :", ivs_headerrow)
    print("i txtgene2genecentric method :", method)
    # set variables 
    ls_outputfile = []
    ds_header = {}
    ddt_indata = {}
    # handle input 
    tivts_value = (ivs_value,)
    i_feature = None
    i_value = len(tivts_key)
    if (ivs_feature != None):
        tivts_value = (ivs_feature,ivs_value,)
        i_feature = len(tivts_key)
        i_value = len(tivts_key) + 1

    # for each input file 
    for s_file in ls_inputfile: 
        # get header row, if not given as input
        if (ivs_headerrow != None): 
            # read out head row
            s_headerrow = None
            i_eof = sum(1 for line in open(s_file))
            i_lineno = 0
            with open(s_file, 'r') as f:
                while (i_lineno < i_eof): 
                    s_line = f.readline()
                    if (type(ivs_headerrow) == int):
                        if (i_lineno == ivs_headerrow):
                            s_headerrow = s_line
                            break
                    elif (type(ivs_headerrow) == str):
                        if (ivs_headerrow in s_line): 
                            s_headerrow = s_line
                            break
                    else:
                        sys.exit('Error: ivs_headerrow '+ivs_headerrow+' is neider integer nor string')
                    i_lineno += 1 
            # update headerrow dictionary
            try: 
                s_catch = ds_header[s_file]
                sys.exit('Error : input file '+s_file+'is more then one time listed in ls_inputfile '+ls_inputfile)
            except KeyError:
                ds_header.update({s_file : s_headerrow})
        # get data 
        # bue 2014-09-02: rubbbisch if tivtsvivs_key occure more then once
        (dt_inputdata,ts_inputxaxis, ts_xaxis_value,ts_xaxis_key,s_xaxis_primarykey,ts_yaxis,dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_file, tivtsvivs_value=tivts_value, tivtsvivs_key=tivts_key, tivtsvivs_primarykey=tivts_key, ivs_labelrow=ivs_labelrow, b_dtl=False)
        # update input data dictionary 
        try: 
            dt_catch = ddt_indata[s_file]
            sys.exit('Error : input file '+s_file+'is more then one time listed in ls_inputfile '+ls_inputfile)
        except KeyError:  
            ddt_indata.update({s_file : dt_inputdata})

    # get measured values per gene  
    dls_center = {}
    for s_file in ddt_indata.keys():
        dt_indata = ddt_indata[s_file]
        for s_genekey in dt_indata.keys(): 
            # get response
            t_response = dt_indata[s_genekey]
            # get gene list
            try: 
                l_center = dls_center[s_genekey]                 
            except: 
                l_center = []
            # update gene list with respnse value 
            l_center.append(t_response[i_value])
            dls_center.update({s_genekey:l_center})
    # get average per gene
    df_center = {} 
    for s_genekey in dls_center.keys():
        # average 2 float
        ls_center = dls_center[s_genekey]
        lf_center = [float(n) for n in ls_center if n != None]
        if (lf_center == []):
            f_average = None
        else: 
            f_average = method(lf_center)
        # put average
        df_center.update({s_genekey : f_average})

    # get gene avergae centered data 
    ddt_outdata = {}
    for s_file in ddt_indata.keys(): 
        dt_indata = ddt_indata[s_file]
        for s_genekey in dt_indata.keys():
            # get response 
            ts_response =  ddt_indata[s_file][s_genekey]
            s_measure = ts_response[i_value]
            # s_measure 2 float
            try:
                f_measure = float(s_measure)
            except TypeError:   # TypeError, ValueError
                f_measure = None 
            # get center 
            f_center = df_center[s_genekey]
            # get average 
            if (f_measure == None) or (f_center == None):
                f_value = None 
            else: 
                f_value = f_measure - f_center
            # get output response
            ls_response = list(ts_response)
            ls_response[i_value] = f_value
            # put output data 
            try: 
                t_outdata = ddt_outdata[s_file][s_genekey]
                sys.exit("Error : puting the outputdictionary together, s_file "+s_file+" s_gene "+s_genekey+" combination occures more then once" )
            except KeyError:
                t_outdata = tuple(ls_response)
                try:  
                    dt_outdata = ddt_outdata[s_file]
                except KeyError:
                    dt_outdata = {}
                dt_outdata.update({s_genekey : t_outdata})
                ddt_outdata.update({s_file : dt_outdata})

    # output files
    for s_infile in ddt_outdata.keys():
        # get filename 
        s_outfile = s_infile
        s_outfile = s_outfile.replace('_rdb.txt','_centric')
        s_outfile = s_outfile.replace('_glue.txt','_centric')
        s_outfile = s_outfile.replace('_igvtools_glue.igv','_centric')
        # igv track 
        s_mode ='w' 
        # write header into output file
        if (ivs_headerrow != None):
            s_mode = 'a' # reset write mode
            s_out = s_outfile+'_glue.txt'
            f = open(s_out, 'w')
            f.write(ds_header[s_infile]) 
            f.close()
        # write data  dictionary into output file
        dt_outdata = ddt_outdata[s_infile]
        lt_outdata = gl.pydt2pylt(dt_matrix=dt_outdata, lt_matrix=[])
        lt_outdata = gl.pylt2pop(lt_matrix=lt_outdata, i_pop=0)
        s_out = gl.pylt2txtrdb(s_outputfile=s_outfile, lt_matrix=lt_outdata, t_xaxis=ts_inputxaxis, s_mode=s_mode)
        # if igv track
        if ('_glue.igv' in s_infile):
            s_out = txtigv2toolsortigv(s_file=s_outfile,)
        # update outputfile list
        ls_outputfile.append(s_out)
    # output
    ls_outputfile.sort()
    print("o txtgene2genecentric ls_outputfile :", ls_outputfile)
    return(ls_outputfile)



# 2 spre
def txtgene2txtspre(ts_inputfile=[], ts_inputlabel=None, tivts_key=('gene_id','feature'), ivs_value='value', ivs_labelrow=None, s_outputfile='genetrackspre', b_intersection=True):
    """
    txtgene2txtspre - convert a set of gene tracks into txt spre format 
    input :
        ls_inputfile : list of gene tack file names or list of igv track file names 
        tivts_key : tupe of string or integer points to the column holding the ('gene_id') or pointing to the columns holding the ('chomosome','start','end',) key. first colum ins 0.
        ivs_value : string or integer pointing to the column holding the 'value'
        ivs_labelrow : integer or string or None. first row is 0. default None is no label row 
        s_outputfile : outputfile name pefix 
    output : 
        s_out : generated txt spre file name
    """
    print("i txtgene2txtspre ts_inputfile", ts_inputfile)
    print("i txtgene2txtspre ts_inputlabel", ts_inputlabel)
    print("i txtgene2txtspre tivts_key", tivts_key)
    print("i txtgene2txtspre ivs_value", ivs_value)
    print("i txtgene2txtspre ivs_labelrow", ivs_labelrow)
    print("i txtgene2txtspre s_outputfile", s_outputfile)
    print("i txtgene2txtspre b_intersection", b_intersection)
    # set output 
    s_out = None
    # set gene key
    es_geneprimary = set()
    es_geneintersection = None
    # get data
    ddt_indata = {}
    for s_file in ts_inputfile:     
        (dt_indata, ts_inputxaxis, ts_inputxaxis_value, ts_inputxaxis_key, s_inputxaxis_primarykey, ts_yaxis,dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_file, tivtsvivs_value=ivs_value, tivtsvivs_key=tivts_key, tivtsvivs_primarykey=tivts_key, ivs_labelrow=ivs_labelrow, b_dtl=False)
        # add data to the object
        try:
            dt_catch = dt_indata[s_file]
            sys.exit("Error : s_file "+s_file+" occure more then one time in ls_inputfile")
        except KeyError:
            ddt_indata.update({s_file : dt_indata})
            # get gene primary key
            for s_geneprimary in  dt_indata.keys(): 
                es_geneprimary.add(s_geneprimary)
            # get gene intersetcion key  
            if (es_geneintersection == None): 
                es_geneintersection = es_geneprimary
            else: 
                es_genesection = set()
                for s_geneprimary in es_geneprimary:
                    if (s_geneprimary in es_geneintersection):
                        es_genesection.add(s_geneprimary)
                es_geneintersection = es_genesection
    # get intersection or combinedset
    if (b_intersection): 
        es_geneprimary = es_geneintersection
    # put label together 
    ls_label = ['primarykey',]
    ls_label.extend(list(ts_inputxaxis_key))
    if (ts_inputlabel == None):
        ls_inputlabel = list(ts_inputfile)
    else: 
        ls_inputlabel = list(ts_inputlabel)
    ls_label.extend(ls_inputlabel)
    # put data togeter 
    dt_spre = {}
    ls_geneprimary = list(es_geneprimary)
    ls_geneprimary.sort()
    # for each y gene 
    for s_geneprimary in ls_geneprimary:
        b_genekey = True 
        ls_line = []
        # for x each sample 
        for s_file in ts_inputfile:
            ls_catch = list(ddt_indata[s_file][s_geneprimary])
            # get gene key parts
            ls_genekey =[]
            for i in range(len(ts_inputxaxis_key)): 
                ls_genekey.append(ls_catch.pop(0))
            if (b_genekey): 
                ls_line.extend(ls_genekey)
                b_genekey = False
            else: 
                pass 
            # get value  
            ls_line.append(ls_catch.pop(0))
            # error check 
            if (len(ls_catch) != 0): 
                sys.exit("Error : s_file "+s_file+", s_geneprimary "+s_geneprimary+" shows iregular content "+ls_catch) 
        dt_spre.update({s_geneprimary : ls_line})
    # write
    s_out = gl.pydt2txtrdb(s_outputfile=s_outputfile, dt_matrix=dt_spre, t_xaxis=tuple(ls_label), s_mode='w')
    # end
    print("o txtgene2txtspre s_out :", s_out)
    return(s_out)



### 2 gene ###
def txtigv2txtgene(s_inputfile, s_geneome='ucsc_grch37hg19_refgene.tsv',ivs_chromosome='chrom',ivs_start='txStart',ivs_end='txEnd',ivs_genomegene='name2',ivs_genomefeature=None, ts_outputlable=('gene_id','feature','value',),s_outputfile='genetrack'):
    """
    txtigv2txtgene - translate igv track compatible files into rdb gene track compatibe txt files 
    input :
        s_inputfile : igv compatibel input file  

        s_geneome : ucsc reference s_inputgene compatible geneome file  
        ivs_chromosome : points to the chromosome column inside the reference genome file
        ivs_start : points to the tx translation region start column inside the reference genome file 
        ivs_end : points to the tx translation region stop column inside the reference genome file
        ivs_genomegene : points to the gene identifier column inside the refernce geneome file
        s_genomefeature : points to the featre column inside the genomefile wich can be a genomeid or such. set to None the igv feature column will be taken as output feature.    

        ts_outputlable : output file column names of the output file  
        s_outputfile : output file name prefix string  
    output : 
        lt_gene : list of tuple with same content as in the gene track file output
        gene track file 
    note :
    general information about the igv file format can be found here: http://www.broadinstitute.org/software/igv/IGV
    ucsc reference genome versions can be downloaded from here: http://genome.ucsc.edu/cgi-bin/hgTables?command=start
    """  
    print("i txtigv2txtgene s_inputfile:", s_inputfile)
    print("i txtigv2txtgene s_geneome:", s_geneome)
    print("i txtigv2txtgene ivs_chromosome:", ivs_chromosome)
    print("i txtigv2txtgene ivs_start:", ivs_start)
    print("i txtigv2txtgene ivs_end:", ivs_end)
    print("i txtigv2txtgene ivs_genomegene:", ivs_genomegene)
    print("i txtigv2txtgene ivs_genomefeature:", ivs_genomefeature)
    print("i txtigv2txtgene ts_outputlable:", ts_outputlable)
    print("i txtigv2txtgene s_outputfile:", s_outputfile)
    # manipulate input variables 
    s_outputfile = s_outputfile+'_gene'
    # get inputfile label row
    ivs_labelrow = 1  # bue 20140802: should become more sophisticated, like the search for something ^chN or kick #track and #type line, wich because of the # get abyway kicked
    # read  igv input file 
    # bue : b_dtl have to be False an igv inputfile shoul never store two different feature value entries at the same chromosome txstart txstop position. txtrdb2pydt and will break.
    tivtsvivs_value = (4,3)  # value,feature
    tivtsvivs_key =(0,1,2,)  # chrom,txstart,txend
    (dt_igv, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_inputfile, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_key, ivs_labelrow=ivs_labelrow, b_dtl=False) 
    # read genome file
    # bue : b_dtl have to be True, one chromosoem txstart txstop position may be labeld by to different gene identifier. 
    if (ivs_genomefeature == None):
        tivtsvivs_value = (ivs_genomegene,)
        tivtsvivs_key = (ivs_chromosome,ivs_start,ivs_end,)
    else: 
        tivtsvivs_value = (ivs_genomegene,ivs_genomefeature)
        tivtsvivs_key = (ivs_chromosome,ivs_start,ivs_end,)
    (dt_genome, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_geneome, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_key, ivs_labelrow=0, b_dtl=True) 
    # merge to gene object 
    dt_gene = {}
    for s_key in dt_igv.keys():
        # get igv dictionary line
        ts_igv = dt_igv[s_key]
        s_chromosome = ts_igv[0]  # string
        s_start =  ts_igv[1]  # string
        s_end =  ts_igv[2]  # string
        s_value = ts_igv[3]  # string value
        s_igvfeature = ts_igv[4]  # string feature igv
        # get geneome dictionary line 
        ts_genome = dt_genome[s_key]
        s_genomefeature = str(ts_genome[3])  # this is a list which gets converted to a string  
        ls_genomegene = ts_genome[3]  # this is a list some positon posess more then one gene identifier
        # get feature
        if (ivs_genomefeature == None):
            s_feature = s_igvfeature
        else: 
            s_feature = s_genomefeature
        # put output into dictionary 
        # bue: it might be that one gene identifer owns two different feature and value entry as splicing variant or such.  
        for s_genomegene in ls_genomegene: 
            t_out = (s_genomegene, s_feature, s_value)
            try:
                t_entry = dt_gene[s_genomegene]
                if (t_entry != t_out): 
                    sys.exit("Error: "+s_genomegene+"have two differnet entryes in the igv file, "+t_entry+", "+t_out+". This issue can not be resolved automatically.")
            except KeyError: 
                pass
            dt_gene.update({s_genomegene : t_out})
    # write  gene  dictionary into output file 
    lt_gene = gl.pydt2pylt(dt_matrix=dt_gene, lt_matrix=[])
    lt_genee = gl.pylt2pop(lt_matrix=lt_gene, i_pop=0)
    gl.pylt2txtrdb(s_outputfile=s_outputfile, lt_matrix=lt_genee, t_xaxis=ts_outputlable, s_mode='w')
    # output
    print("o txtigv2txtgene lt_gene:", lt_gene)
    return(lt_gene)



### 2 igv ###
#s_genome = 'ucsc_ncbi36hg18_refgene.tsv' # hg18
#s_genome = 'ucsc_grch37hg19_refgene.tsv' # hg19
#s_track_format='name="track_name" description="track_description" visibility=full color=255,000,000 altColor=000,000,255 midRange=-0.001:0.001 midColor=000,255,000 priority=128 autoScale=off gridDefault=on maxHeightPixels=128:32:8 graphType=bar viewLimits=-3:3 yLineMark=0.0 yLineOnOff=on windowingFunction=none smoothingWindow=off url=http://hweb.ohsu.edu/ coords=0 scaleType=linear gffTags=off \n'  # example

def txtgene2txtigv(s_inputfile,ivs_inputgene='gene_id',ivs_inputfeature='feature',ivs_inputvalue='value', s_geneome='ucsc_grch37hg19_refgene.tsv',ivs_chromosome='chrom',ivs_start='txStart',ivs_end='txEnd',ivs_genomegene='name2', s_track_name='track_name', s_track_description='track_description', s_track_format='visibility=full color=255,000,000 altColor=000,000,255 midRange=-0.001:0.001 midColor=000,255,000 priority=128 autoScale=off gridDefault=off maxHeightPixels=128:32:8 graphType=bar viewLimits=-3:3 yLineMark=0.0 yLineOnOff=on windowingFunction=none smoothingWindow=off url=http://hweb.ohsu.edu/ coords=0 scaleType=linear gffTags=off', s_outputfile='igvtrack'): 
    """
    txtgene2igvtrack - translate rdb gene track compatibe txt files into igv track compatibe files 
    dependency: 
        igvtools installed which can be downloaded here: http://www.broadinstitute.org/software/igv/download
    input : 
        s_inputfile : rdbxgene compatibel input file  
        s_inputgene : points to the gene identifier column inside the inputfile 
        s_inputfeature : points to the featre column inside the inpufile. set to None if no feature column inexistent.    
        s_inputvalue : points to the value column inside the inputfile 

        s_geneome : ucsc reference s_inputgene compatible geneome file  
        s_chromosome : points to the chromosome column inside the reference genome file
        s_start : points to the tx translation region start column inside the reference genome file 
        s_end : points to the tx translation region stop column inside the reference genome file
        s_genomegene : points to the gene identifier column inside the refernce geneome file  

        s_track_name : string trackname 
        s_track_description : string track description   
        s_track_format : track line format specification, detailed infromation can be found here: http://www.broadinstitute.org/software/igv/TrackLine
        s_outputfile : file name output prefix string  
    output : 
        s_out : generated igv track file name
        igv track file 
    note: 
    general information about the igv file format can be found here: http://www.broadinstitute.org/software/igv/IGV
    ucsc reference genome versions can be downloaded from here: http://genome.ucsc.edu/cgi-bin/hgTables?command=start
    """
    print("i txtgene2txtigv s_inputfile", s_inputfile)
    print("i txtgene2txtigv ivs_inputgene", ivs_inputgene)
    print("i txtgene2txtigv ivs_inputfeature", ivs_inputfeature)
    print("i txtgene2txtigv ivs_inputvalue", ivs_inputvalue)
    print("i txtgene2txtigv s_geneome", s_geneome)
    print("i txtgene2txtigv ivs_chromosome", ivs_chromosome)
    print("i txtgene2txtigv ivs_start", ivs_start)
    print("i txtgene2txtigv ivs_end", ivs_end)
    print("i txtgene2txtigv ivs_genomegene", ivs_genomegene)
    print("i txtgene2txtigv s_track_name", s_track_name)
    print("i txtgene2txtigv s_track_description", s_track_description)
    print("i txtgene2txtigv s_track_format", s_track_format)
    print("i txtgene2txtigv s_outputfile", s_outputfile)
    # set output variable
    s_out = None
    # manipulate input variables 
    s_outputfile = s_outputfile+'_igv'
    # read  input file
    if (ivs_inputfeature == None): 
        tivtsvivs_value = (ivs_inputvalue,)
    else: 
        tivtsvivs_value = (ivs_inputvalue, ivs_inputfeature)
    (dt_input, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_inputfile, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=ivs_inputgene, tivtsvivs_primarykey=ivs_inputgene, ivs_labelrow=0, b_dtl=False) 
    # read genome
    (dt_genome, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_geneome, tivtsvivs_value=ivs_genomegene, tivtsvivs_key=(ivs_chromosome,ivs_start,ivs_end,), tivtsvivs_primarykey=(ivs_chromosome,ivs_start,ivs_end,), ivs_labelrow=0, b_dtl=True) 
    # merge to igv obj
    dt_igv = {}
    for s_key in dt_genome.keys():
        # get genome dictionary line  
        ts_genome = dt_genome[s_key]
        s_chromosome = ts_genome[0][0]  # string
        s_start =  ts_genome[1][0]  # string
        s_end =  ts_genome[2][0]  # string
        s_feature = str(ts_genome[3])  # this is a list which gets converted to a string  
        ls_gene = ts_genome[3]  # this is a list some positon posess more then one gene identifier
        # get input dictionary line 
        s_value = None
        b_flag = False
        for s_gene in ls_gene: 
            try: 
                ts_input = dt_input[s_gene]
                if (b_flag): 
                    sys.exit('Error: s_geneome position '+s_chromosome+' '+s_start+' '+s_end+' host two gene identifier, both of this identifer are present in the s_inputfile. this issue can not automatically beresolved.')
                b_flag = True
                s_value = ts_input[1]
                if (ivs_inputvalue != None):
                    s_feature = s_feature +'<'+ts_input[2]  # string overwrites standard s_feature
                # put output igv dictionary line 
                s_keyigv  = s_chromosome+'_'+s_start+'_'+s_end
                t_entryigv = (s_chromosome,s_start,s_end,s_feature,s_value,)
                dt_igv.update({s_keyigv : t_entryigv})
            except KeyError: 
                pass 
    # fuse track line
    s_track = '#track name="'+s_track_name+'" description="'+s_track_description+'" '+s_track_format+'\n'
    # write igv track format into output file 
    f = open(s_outputfile+'_glue.txt', 'w')
    f.write(s_track) 
    f.close()
    # write igv dictionary into output file 
    lt_igv = gl.pydt2pylt(dt_matrix=dt_igv, lt_matrix=[])
    lt_igvv = gl.pylt2pop(lt_matrix=lt_igv, i_pop=0)
    gl.pylt2txtrdb(s_outputfile=s_outputfile, lt_matrix=lt_igvv, t_xaxis=('chromosome','start','end','feature','value'), s_mode='a')
    # glue to igv
    s_out = txtigv2toolsortigv(s_file=s_outputfile)
    # ende
    print("o txtgene2txtigv s_out", s_out)
    return(s_out)



### 2 circos ###
def txtigv2txtcircos(s_inputfile, s_circoschromosoneprefix='hs', s_outputfile='circostrack'):
    """
    txtigv2txtcircos - translate igv track compatible files into circos track compatibe data files 
    input :
        s_inputfile : igv compatibel input file  
        s_circoschromosoneprefix : 
        s_outputfile : output file name prefix string  
    output : 
        lt_circos : list of tuple with same content as in the circos track data file output
        circos track data file 
    note: 
    general information about the igv file format can be found here: http://www.broadinstitute.org/software/igv/IGV
    general information about the circos file format can be found here: http://www.circos.ca/documentation/tutorials/configuration/data_files/
    """
    print("i txtigv2txtcircos s_inputfile :", s_inputfile)
    print("i txtigv2txtcircos s_circoschromosoneprefix :", s_circoschromosoneprefix)
    print("i txtigv2txtcircos s_outputfile:", s_outputfile)
    # manipulate input variables 
    s_outputfile = s_outputfile+'_circos'
    # get inputfile label row
    ivs_labelrow = 1  # bue 20140802: should become more sophisticated, like the search for something ^chN or kick #track and #type line, wich because of the # get abyway kicked
    # read  igv input file 
    # bue : b_dtl have to be False an igv inputfile shoul never store two different feature value entries at the same chromosome txstart txstop position. txtrdb2pydt and will break.
    tivtsvivs_value = (4,3)  # value,feature
    tivtsvivs_key =(0,1,2,)  # chrom,txstart,txend
    (dt_igv, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_inputfile, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_key, ivs_labelrow=ivs_labelrow, b_dtl=False) 
    # merge to igv obj
    lt_circos = []
    ls_key = list(dt_igv.keys())
    ls_key.sort()
    ts_key = tuple(ls_key)
    for s_key in ts_key:
        # get genome dictionary line  
        ts_igv = dt_igv[s_key]
        s_chromosome = ts_igv[0] # string
        s_start =  ts_igv[1]  # string
        s_end =  ts_igv[2]  # string
        s_value = ts_igv[3]  # string
        s_feature = ts_igv[4]  # string 
        # manipulate chromosome string 
        s_chr = re.sub('chr',s_circoschromosoneprefix, s_chromosome)
        # put output tuple
        t_out = (s_chr, s_start, s_end, s_value)
        lt_circos.append(t_out)
    # write file 
    gl.pylt2txtrdb(s_outputfile=s_outputfile, lt_matrix=lt_circos, t_xaxis=None, s_mode='w')
    # output
    print("o txtigv2txtcircos lt_circos :", lt_circos)
    return(lt_circos)



# module self test  
if (__name__=='__main__'):
    # centric
    print("selftest: txt gene 2 gene centric") 
    ls_inputfile = ['jwgray_bccl_rnaseqalexa_21MT1_log2_glue.txt','jwgray_bccl_rnaseqalexa_21MT2_log2_glue.txt','jwgray_bccl_rnaseqalexa_184A1_log2_glue.txt','jwgray_bccl_rnaseqalexa_184B5_log2_glue.txt',]
    tivts_key = ('gene_id',) 
    ivs_value = 'value'
    ivs_feature = 'feature'
    method = statistics.mean
    txtgene2genecentric(ls_inputfile=ls_inputfile, tivts_key=tivts_key, ivs_value=ivs_value, ivs_feature=ivs_feature, ivs_labelrow=0, ivs_headerrow=None, s_outputfile='selftest_txtgene2genecentric', method=method)

    # 2 spre
    print("selftest: txt gene 2 txt spre") 
    ts_inputfile = ('jwgray_bccl_rnaseqalexa_21MT1_log2_centric_glue.txt','jwgray_bccl_rnaseqalexa_21MT2_log2_centric_glue.txt','jwgray_bccl_rnaseqalexa_184A1_log2_centric_glue.txt','jwgray_bccl_rnaseqalexa_184B5_log2_centric_glue.txt',)
    ts_inputlabel = ('21MT1','21MT2','184A1','184B5',)
    txtgene2txtspre(ts_inputfile=ls_inputfile, ts_inputlabel=ts_inputlabel, tivts_key=('gene_id','feature'), ivs_value='value', ivs_labelrow=0, s_outputfile='selftest_txtgene2txtspre',  b_intersection=True)

    # 2 igv 
    print("selftest: txt gene 2 txt igv and txt igv 2 tool sort igv") 
    s_inputfile = 'drugbank3illicit_gene_glue.txt'
    s_track_name ='drugbank_v3_illicit'
    s_track_description ='nop'
    s_track_format='visibility=full color=255,000,000 altColor=000,000,255 midRange=-0.001:0.001 midColor=000,255,000 priority=128 autoScale=off gridDefault=off maxHeightPixels=128:32:8 graphType=bar viewLimits=-3:3 yLineMark=0.0 yLineOnOff=on windowingFunction=none smoothingWindow=off url=http://hweb.ohsu.edu/ coords=0 scaleType=linear gffTags=off' 
    s_track = txtgene2txtigv(s_inputfile=s_inputfile,ivs_inputgene='gene_id',ivs_inputfeature='feature',ivs_inputvalue='value', s_geneome='ucsc_grch37hg19_refgene.tsv',ivs_chromosome='chrom',ivs_start='txStart',ivs_end='txEnd', ivs_genomegene='name2', s_track_name=s_track_name, s_track_description=s_track_description, s_track_format=s_track_format, s_outputfile='selftest_txtgene2txtigv') #s_outputfile='drugbank3illicit'
    print("selftest: txt igv 2 txt gene") 
    s_inputfile = 'selftest_txtgene2txtigv_igv_igvtools_glue.igv'
    lt_gene = txtigv2txtgene(s_inputfile=s_inputfile, s_geneome='ucsc_grch37hg19_refgene.tsv',ivs_chromosome='chrom',ivs_start='txStart',ivs_end='txEnd',ivs_genomegene='name2',ivs_genomefeature=None, ts_outputlable=('gene_id','feature','value',),s_outputfile='selftest_txtigv2txtgene') #'feature'

    # 2 gene 
    print("selftest: txt igv 2 txt gene") 
    s_inputfile = 'selftest_txtgene2txtigv_igv_igvtools_glue.igv'
    txtigv2txtgene(s_inputfile=s_inputfile, s_geneome='ucsc_grch37hg19_refgene.tsv',ivs_chromosome='chrom',ivs_start='txStart',ivs_end='txEnd',ivs_genomegene='name2',ivs_genomefeature=None, ts_outputlable=('gene_id','feature','value',),s_outputfile='genetrack')

    # 2 cicros
    print("selftest: txt igv 2 txt circos") 
    s_inputfile = 'selftest_txtgene2txtigv_igv_igvtools_glue.igv'
    lt_circos = txtigv2txtcircos(s_inputfile, s_circoschromosoneprefix='hs', s_outputfile='selftest_txtigv2txtcircos')
